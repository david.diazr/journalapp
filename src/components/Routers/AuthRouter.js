import React from 'react'
import { LoginScreen } from '../auth/LoginScreen'
import { RegisterScreen } from '../auth/RegisterScreen'
import { Routes, Route, BrowserRouter } from "react-router-dom";

export const AuthRouter = () => {
  return (

      <Routes>
        <Route path="/" element={<h1>Auth Screen</h1>} >
        <Route index path="login" element={<LoginScreen />} />
        <Route path="register" element={<RegisterScreen />} />
        </Route>
      </Routes>
    
  )
}
