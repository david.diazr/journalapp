import React from 'react'
import { JournalScreen } from '../journal/JournalScreen'
import { Routes, Route, BrowserRouter } from "react-router-dom";
import { AuthRouter } from './AuthRouter'
import { LoginScreen } from '../auth/LoginScreen';

export const AppRouter = () => {
  return (

    <BrowserRouter>
        <Routes>
          <Route path="/auth/*" element={<AuthRouter/>} />
          <Route path="/" element={<LoginScreen />} />
        </Routes>
    </BrowserRouter>

  )
}
