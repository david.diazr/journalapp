import React from 'react';
import ReactDOM from 'react-dom/client';

import { JournalScreen } from './components/journal/JournalScreen';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <JournalScreen />
);
